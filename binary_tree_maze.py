import random
import os
import time
from typing import List


class Cell:

    def __init__(self, row: int, column: int) -> None:
        self.row = row
        self.column = column
        self.north = None
        self.south = None
        self.east = None
        self.west = None
        self.visited = False
        self.linked_cell = None

    def link_cell(self, cell: 'Cell'):
        self.linked_cell = cell


    def is_linked(self, cell):
        return cell == self.linked_cell

    def __str__(self) -> str:
        return f"Cell: {self.row}:{self.column}"


class Grid:
    def __init__(self, rows: int, columns: int) -> None:
        self.rows = rows
        self.columns = columns
        self.grid = self.prepare_grid()
        self.path = []
        self.configure_cell()

    def prepare_grid(self):
        return [[Cell(row, col) for col in range(self.columns)] for row in range(self.rows)]

    def each_row(self):
        for row in self.grid:
            yield row

    def each_cell(self):
        for row in self.each_row():
            for cell in row:
                if cell:
                    yield cell

    def configure_cell(self):
        for cell in self.each_cell():
            row = cell.row
            col = cell.column
            cell.north = self.access_grid(row-1, col)
            cell.south = self.access_grid(row+1, col)
            cell.west = self.access_grid(row, col-1)
            cell.east = self.access_grid(row, col+1)

    def access_grid(self, row: int, column: int):
        if row >= 0 and row <= self.rows - 1 and column >= 0 and column <= len(self.grid) - 1:
            return self.grid[row][column]
        return None

    def __str__(self) -> str:
        output = "+" + "---+" * self.columns + "\n"
        for row in self.each_row():
            top = "|"
            bottom = "+"
            for cell in row:
                if cell == None:
                    cell = Cell(-1,-1)
                body = "   "
                if cell in self.path:
                    body = " . "
                east_boundary = " " if cell.is_linked(cell.east) else "|"
                
                top += body + east_boundary
                south_boundary = "   " if cell.is_linked(cell.south) else "---"
                
                corner = "+"

                bottom += south_boundary + corner
            output += top + "\n"
            output += bottom + "\n"
        return output

class BinaryTree:
    def __init__(self, grid: 'Grid') -> None:
        self.grid = grid

    def binary_tree(self):
        for cell in self.grid.each_cell():
            neighbors = []
            if cell.south:
                neighbors.append(cell.south)
            if cell.east:
                neighbors.append(cell.east)
            if len(neighbors) > 0:
                idx = random.randrange(0, len(neighbors))
                neighbor = neighbors[idx]
                if neighbor:
                    cell.link_cell(neighbor)
                #for c in cell.links:
                print(f"LINKS FOR:{cell} -> {cell.linked_cell}")
                print("\n")
                #print(self.grid)
                #time.sleep(0.1)
                #os.system('clear')
    def find_path(self, start: Cell, goal: Cell) -> List[Cell]: 
        stack = [(start, [])]
        while stack:
            current_cell, path = stack.pop()
            current_cell.visited = True
            path.append(current_cell)

            if current_cell == goal:
                self.grid.path = path
                return path

            #for neighbor in current_cell.get_links():
            if current_cell.link_cell:
                if not current_cell.linked_cell.visited:
                    stack.append((current_cell.linked_cell, path[:]))
        return []  # No path found

##### Demo #####
grid = Grid(10,10)
bt = BinaryTree(grid)
bt.binary_tree()
path = bt.find_path(grid.access_grid(5,2),grid.access_grid(9,9))

print(grid)

