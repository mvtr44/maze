-- |
-- Module      : Maze
-- Description : Ein Modul zum Erstellen und Konfigurieren eines Labyrinths
--
-- Dieses Modul stellt Funktionen zum Erstellen und Konfigurieren eines Labyrinths bereit.
-- Es enthält auch Funktionen zur Benutzerinteraktion und zur Anzeige des Labyrinths.
module Menu (showMenu, parseMenuInput) where

import Control.Concurrent
import Maze
import System.Console.ANSI
import System.Exit

-- | Gibt das Hauptmenü auf der Konsole aus.
showMenu :: Grid Cell -> IO ()
showMenu m = do
  putStrLn "---------------------------------------"
  putStrLn "1 - Define Start Point For Path"
  putStrLn "2 - Show all Paths"
  putStrLn "3 - Quit"
  input <- getLine
  parseMenuInput input m

-- | Verarbeitet die Benutzereingabe aus dem Hauptmenü
parseMenuInput :: String -> Grid Cell -> IO ()
parseMenuInput "1" = defineStart
parseMenuInput "2" = showAllPaths
parseMenuInput "3" = stopProgram
parseMenuInput _ = invalidInput

-- | Behandelt ungültige Benutzereingabe
invalidInput :: Grid Cell -> IO ()
invalidInput m = do
  putStrLn "invalid input"
  showMenu m

-- | Beendet das Programm
stopProgram :: Grid Cell -> IO ()
stopProgram _ = do
  putStrLn "Stopping the program"
  exitSuccess

-- | Definiert den Startpunkt für den Pfad
defineStart :: Grid Cell -> IO ()
defineStart m = do
  putStrLn "Setting Start Point"
  let maxIdx = size m
  putStrLn $ "Enter Row " ++ "max. (Index: " ++ show maxIdx ++ "):"
  inputRow <- getLine
  putStrLn $ "Enter Column " ++ "max. (Index: " ++ show maxIdx ++ "):"
  inputCol <- getLine
  let row = (read inputRow :: Int)
  let col = (read inputCol :: Int)
  if row > maxIdx || col > maxIdx
    then do
      putStrLn "Index out of bounds"
      showMenu m
    else do
      let start = getStartCell row col m
      let goal = last $ concat (grid m)
      let p = findPath m start goal
      print m {path = p}
      showMenu m

-- | Zeigt alle Pfade im Labyrinth an
showAllPaths :: Grid Cell -> IO ()
showAllPaths m = do
  let indices = [(row, col) | row <- [0 .. size m - 1], col <- [0 .. size m - 1]]
  mapM_ (printMaze m) indices
  showMenu m

-- | Zeigt dan Pfad im Labyrinth für eine bestimmte Zeile und Spalte an
printMaze :: Grid Cell -> (Int, Int) -> IO ()
printMaze m (row', col') = do
  let goal = last $ concat (grid m)
  let start = getStartCell row' col' m
  let p = findPath m start goal
  print m {path = p}
  threadDelay 500000
  clearScreen