{-# LANGUAGE FlexibleInstances #-}
{-|
Module      : Main
Description : Das Hauptmodul des Labyrinth-Programms

Dieses Modul enthält die `main`-Funktion, die das Labyrinth-Programm startet.
Es initialisiert das Labyrinth basierend auf Benutzereingaben und ruft das Hauptmenü auf.
-}
module Main where

import Maze
import Menu
import System.Console.ANSI
import System.Random (newStdGen,randoms)

-- | Die `main`-Funktion startet das Labyrinth-Programm
main :: IO ()
main = do
  g <- newStdGen
  clearScreen
  setSGR
    [ SetConsoleIntensity BoldIntensity,
      SetColor Foreground Vivid Cyan
    ]
  putStrLn "**************************************************"
  putStrLn "*                 MAZE MENU                      *"
  putStrLn "**************************************************"
  setSGR [Reset]
  putStrLn "Enter Maze Size: "
  input <- getLine
  let size = (read input :: Int)
  let dims = size * size
  let nums = take dims $ fmap round (randoms g :: [Double])
  let m = binaryTreeMaze nums (configureMaze (initGrid size))
  clearScreen
  print m
  showMenu m
