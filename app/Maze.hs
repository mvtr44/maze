{-# LANGUAGE FlexibleInstances #-}

{-|
Module      : Maze
Description : Ein Modul zum Erstellen und Konfigurieren eines Labyrinths

Dieses Modul stellt Funktionen zum Erstellen und Konfigurieren eines Labyrinths bereit. 
Es enthält auch Datenstrukturen, die für die Darstellung des Labyrinths verwendet werden.
-}
module Maze where

import Data.Maybe (fromJust, isJust)


-- | Die Cell Datenstruktur, die eine Zelle im Labyrinth repräsentiert. 
data Cell = Cell
  { row :: Int, -- ^ 
    column :: Int,
    north :: Maybe Cell, -- ^ nörtliche Nachbarzelle 
    south :: Maybe Cell,-- ^ südliche Nachbarzelle
    east :: Maybe Cell,  -- ^ östliche Nachbarzelle
    west :: Maybe Cell, -- ^ westliche Nachbarzelle
    linkedCell :: Maybe Cell -- ^ verlinkte Zelle
  }

instance Show Cell where
  show (Cell row col _ south east _ links) = 
    "Cell: " ++ show row ++ ":" ++ show col ++ " S: (" ++ show south ++ ") E: (" ++ show east ++ ") Links: " ++ show links

instance Eq Cell where
  (==) cell1 cell2 = row cell1 == row cell2 && column cell1 == column cell2

-- | Die Grid Datenstruktur, repräsentiert das Labyrinth.
data Grid a = Grid
  { size :: Int, -- ^ Größe des Labyrinths (Anzahl der Zellen in einer Zeile/Spalte)
    grid :: [[a]], -- ^ Die 2D Arrays des Labyrinths
    path :: [a] -- ^ Der Pfad durch das Labyrinth
  }

instance Functor Grid where
  fmap f (Grid size grid path) = Grid size (map (map f) grid) []

instance Show (Grid Cell) where
  show (Grid size grid path) =
    "+"
      ++ concat ["---+" | _ <- [1 .. size]]
      ++ "\n"
      ++ concatMap showRow grid
    where
      showRow rows =
        let (top, bottom) = foldMap showCell rows
         in "|" ++ top ++ "\n" ++ "+" ++ bottom ++ "\n"
      showCell cell = (east_boundary, south_boundary ++ corner)
        where
          east_boundary = body ++ if isLinked cell (east cell) then " " else "|"
          south_boundary = if isLinked cell (south cell) then "   " else "---"
          corner = "+"
          body = if cell `elem` path then 
            if cell == last path then 
                red_bold_color ++ " . " ++ reset_color else cyan_bold_color ++ " . " ++ reset_color else "   "
          cyan_bold_color = "\x1b[36m"
          red_bold_color = "\x1b[31;1m"
          reset_color = "\x1b[0m"

-- | Initialisiert ein leeres Labyrinth mit der angegebenen Größe
initGrid :: Int -> Grid Cell
initGrid size = Grid size [[initCell size r c | c <- [0 .. size - 1]] | r <- [0 .. size - 1]] []

-- | Initialisiert eine leere Zelle mit den angegebenen Koordinaten
initCell :: Int -> Int -> Int -> Cell
initCell size r c = Cell r c Nothing Nothing Nothing Nothing Nothing

-- | Konfiguriert das Labyrinth, indem benachbarte Zellen für jede Zelle festgelegt werden
configureMaze :: Grid Cell -> Grid Cell
configureMaze grid = fmap (configureCell grid) grid

-- | Konfiguriert eine Zelle im Labyrinth, indem die benachbarten Zellen festgelegt werden
configureCell :: Grid Cell -> Cell -> Cell
configureCell grid cell =
  let row' = row cell
      col' = column cell
      north' = accessGrid grid (row' - 1) col'
      south' = accessGrid grid (row' + 1) col'
      west' = accessGrid grid row' (col' - 1)
      east' = accessGrid grid row' (col' + 1)
   in cell {north = north', south = south', west = west', east = east'}

-- | Gibt die Zelle im Gitter an der angegebenen Zeile und Spalte zurück
accessGrid :: Grid Cell -> Int -> Int -> Maybe Cell
accessGrid (Grid size grid _) row col =
  if row >= 0 && row <= size - 1 && col >= 0 && col <= size - 1
    then Just ((grid !! row) !! col)
    else Nothing

-- | Überprüft, ob eine Zelle mit einer bestimmten Zelle verknüpft ist
isLinked :: Cell -> Maybe Cell -> Bool
isLinked _ Nothing = False
isLinked (Cell _ _ _ _ _ _ linkedCell) (Just c1) = case linkedCell of
  Just c -> c == c1
  Nothing -> False

-- | Erstellt ein Labyrinth mit dem binären  Binarytree Algorithmus basierend auf einer Zufallszahlenserie
binaryTreeMaze :: [Int] -> Grid Cell -> Grid Cell
binaryTreeMaze nums grid = fmap (binaryTreeCell nums (size grid)) grid

-- | Konfiguriert eine Zelle im Labyrinth mit dem binären Baumalgorithmus basierend auf einer Zufallszahlenserie
binaryTreeCell :: [Int] -> Int -> Cell -> Cell
binaryTreeCell nums size cell =
  let neighbors = buildNeigbors (south cell) (east cell)
   in if not (null neighbors)
        then
          if length neighbors == 1
            then cell {linkedCell = Just $ head neighbors}
            else
              let idx_cell = row cell * size + column cell
               in cell {linkedCell = Just $ neighbors !! (nums !! idx_cell)}
        else cell

-- | Erstellt eine Liste der benachbarten Zellen, falls die mit Just vorhanden sind
buildNeigbors :: Maybe Cell -> Maybe Cell -> [Cell]
buildNeigbors Nothing Nothing = []
buildNeigbors (Just c1) Nothing = [c1]
buildNeigbors Nothing (Just c2) = [c2]
buildNeigbors (Just c1) (Just c2) = c1 : [c2]

-- | Findet den Pfad von einer Startzelle zu einer Zielzelle im Labyrinth, indem die verlinkten Zellen verfolgt werden
findPath :: Grid Cell -> Cell -> Cell -> [Cell]
findPath _ start goal | start == goal = [start]
findPath m start goal = case getNeighbor m (linkedCell start) of
  Just c -> start : findPath m c goal
  Nothing -> []

-- | Gibt einen unbesuchten benachbarten Nachbarn einer Zelle zurück
getNeighbor :: Grid Cell -> Maybe Cell -> Maybe Cell
getNeighbor g (Just ((Cell row' col' _ _ _ _ _))) = Just (head $ filter (\c -> column c == col' && row c == row') (concat (grid g)))
getNeighbor _ Nothing = Nothing

-- | Gibt die Startzelle mit den angegebenen Zeilen- und Spaltenkoordinaten im Labyrinth zurück
getStartCell :: Int -> Int -> Grid Cell -> Cell
getStartCell row' col' g = head $ filter (\cell -> row cell == row' && column cell == col') (concat (grid g))